/**
* @file hwsetup.c
* @brief ハードウェアを初期化する
* @details CPU内部レジスタを初期化した後に実行される
*/
/************************************************************************
*
* Device     : RX/RX600/RX64M
*
* File Name  : hwsetup.c
*
* Abstract   : Hardware Setup file.
*
* History    : 1.00  (2013-05-30)  [Hardware Manual Revision : 0.50]
*
* NOTE       : THIS IS A TYPICAL EXAMPLE.
*
*  Copyright (C) 2013 Renesas Electronics Corporation and
*  Renesas Solutions Corp. All rights reserved.
*
************************************************************************/

#include "iodefine.h"

/**
* @addtogroup TAG_PRC0_WR
* @brief クロック発生回路関連レジスタへの書き込み許可設定
*/
/** @{ */
/**
* @brief 書き込み許可
*/
#define PRC0_WR_ENA 0xA501
/**
* @brief 書き込み禁止
*/
#define PRC0_WR_DIS 0xA500
/** @} */

/**
* @addtogroup TAG_CKSEL
* @brief クロックソース選択ビット
*/
/** @{ */
/**
* @brief LOCO選択
*/
#define CKSEL_LOCO 0x00
/**
* @brief HOCO選択
*/
#define CKSEL_HOCO 0x01
/**
* @brief メインクロック発振器選択
*/
#define CKSEL_MAIN_CLK 0x02
/**
* @brief サブクロック発振器選択
*/
#define CKSEL_SUB_CLK 0x03
/**
* @brief PLL回路選択
*/
#define CKSEL_PLL 0x04
/** @} */

static void InitSystemClock(void);

/**
* @brief ハードウェアを初期化する
* @return なし
* @details ハードウェアを初期化するための関数を呼び出す
* @par 処理手順
*/
void HardwareSetup(void)
{
	//! -# システムクロックを設定する
	InitSystemClock();
}

/**
* @brief システムクロックを初期化する
* @return なし
* @details 初期状態ではCPU内部で発生させたクロックで動作する。@n
*          XTAL/EXTAL端子から入力されたクロックで動作するように設定する。
* @todo MOSCWTCRを設定するべき
* @par 処理手順
*/
static void InitSystemClock(void)
{
	//! -# クロック発生回路関連レジスタのプロテクトを書き込み許可する
	SYSTEM.PRCR.WORD = PRC0_WR_ENA;

	//! -# メインクロック発振開始
	SYSTEM.MOSCCR.BIT.MOSTP = 0;

	//! -# 発振安定フラグが立つのを待つ
	while(SYSTEM.OSCOVFSR.BIT.MOOVF == 0) {
	}

	//! -# システムクロックのソースをメインクロックにする
	SYSTEM.SCKCR3.BIT.CKSEL = CKSEL_MAIN_CLK;

	//! -# クロック発生回路関連レジスタのプロテクトを書き込み禁止する
	SYSTEM.PRCR.WORD = PRC0_WR_DIS; // CKSEL_SUB_CLK; // 
}
