/***********************************************************************/
/*                                                                     */
/*  FILE        :Main.c or Main.cpp                                    */
/*  DATE        :Tue, Oct 31, 2006                                     */
/*  DESCRIPTION :Main Program                                          */
/*  CPU TYPE    :                                                      */
/*                                                                     */
/*  NOTE:THIS IS A TYPICAL EXAMPLE.                                    */
/*                                                                     */
/***********************************************************************/
#include "iodefine.h"

void main(void)
{
	PORTC.PDR.BIT.B0 = 1; // 出力に設定する
	PORTC.PODR.BIT.B0 = 1; // Highを出力する

	for(;;){
		// 無限ループ
	}
}
